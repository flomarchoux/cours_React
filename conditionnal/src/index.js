import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

//Redux
import Reducer from './reducers/store_1'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
const store = createStore(Reducer)

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
)
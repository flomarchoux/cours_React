import ButtonCount2 from "./ButtonCount2";
import React from 'react';

function Counter2(props){
    let button2 = <ButtonCount2  countIncremente={props.increase}
                                 countDecrease={props.decrease}/>
    return(
        <div>
            <p>Vous avez {props.count}</p>
            {button2}
        </div>
    );
}

export default Counter2

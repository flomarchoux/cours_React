function LogoutButton(props){
    function logout(){
        props.setisLogin()
    }
    return(
        <div>
            <button onClick={logout}>LOGOUT</button>
        </div>
    );
}

export default LogoutButton
import '../style/footer.css'
import monkey from "../images/monkey.svg"

function Footer() {
    return (
        <div>
            <footer className="right">Site developed by MonkeyTelecom©</footer>
            <footer className="left"><img src={monkey} width='100px' alt="logo"/></footer>
        </div>
    )
}

export default Footer
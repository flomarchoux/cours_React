function ButtonCount(props){
    function plus(){
        props.countIncremente()
    }
    function moins(){
        props.countDecrease()
    }
    return(
        <div>
            <button onClick={plus}>+</button>
            <button onClick={moins}>-</button>
        </div>

    );

}

export default ButtonCount
import ButtonCount from "./ButtonCount";
import React from 'react';

function Counter(props){
    let [count,setCount] = React.useState(0);
    let button = <ButtonCount  countIncremente={() => setCount(count + 1)}
                               countDecrease={() => setCount(count - 1)}/>
    return(
        <div>
            <p>You clicked {count} times</p>
            {button}
        </div>
    );
}

export default Counter
function UserGreeting(props) {
    return (
        <div>
            <h1>Welcome back!</h1>
            <p>Vous êtes connecté</p>
        </div>
    );
}

function GuestGreeting(props) {
    return (
        <div>
            <h1>Please sign up.</h1>
            <p>Vous êtes déconnecté</p>
        </div>
    );
}

function Greeting(props) {
    const isLoggedIn = props.isLoggedIn;
    if (isLoggedIn) {
        return <UserGreeting />;
    }
    return <GuestGreeting />;
}

export default Greeting
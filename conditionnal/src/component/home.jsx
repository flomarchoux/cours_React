import "../style/home.css"
import logo from "../images/logo.svg"

export default function Accueil() {
    return (
        <div>
            <br/>
            <center>
                <h1>Présentation du Cours</h1>
                <p>Ceci est le site sur les différents exercices vue durant le cour de React JS,</p>
                <p>dans les onglets suivant vous trouverez tous les exercice JSX fait.</p>
                <p>Et dans l'onget "Autre" vous trouverez les autres exercices javascript classique.</p>
                <a href="https://gitlab.com/flomarchoux/cours_React" target='_blank'>
                    <img src={logo} width='400px' height='400px' className="App-logo" alt="logo"/>
                </a>
            </center>
        </div>
    )
}
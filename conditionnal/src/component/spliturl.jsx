import React from 'react';

function SplitUrl(props){
    function split(str){
        if(str === undefined){
            return undefined;
        }
        let url = str.split("/");
        let nb = 0;
        let n =0;
        let url2 = [];
        while  (nb !== url.length){
            if (url[nb] !== ''){
                url2[n] = url[nb];
                n++;
            }
            nb++;
        }
        /*setSplitUrl(url2.join(" "));*/
        return url2.join(" ");
    }
    let [url,setUrl] = React.useState("");
    /*let [splitUrl,setSplitUrl] = React.useState();*/
    return(
        <div>
            <input value={url} onChange={(e) => {setUrl(e.target.value)}}/>
            {/*<button onClick={() => split(url)}>SPLIT</button>*/}
            <br/>
            {/*{splitUrl}*/}
            l'URL divisé donne :
            <br/>
            {split(url)}
        </div>
    );

}

export default SplitUrl
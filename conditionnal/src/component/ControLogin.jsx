import React from 'react';
import LogoutButton from "./ButtonLogout";
import LoginButton from "./ButtonLogin";
import Greeting from "./Greeting";

function LoginControl(props){
    let [isLoggedIn,SetisLogin] = React.useState(false);
    let button;
    if (isLoggedIn){
        button = <LogoutButton  setisLogin={() => SetisLogin(false)} />
    }
    else{
        button = <LoginButton setIsLogin={() => SetisLogin(true)}/>
    }
    return(
        <div>
            <Greeting isLoggedIn = {isLoggedIn} />
            {button}
        </div>
    );
}

export default LoginControl
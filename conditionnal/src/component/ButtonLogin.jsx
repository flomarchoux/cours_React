function LoginButton(props){
    function login(){
        props.setIsLogin()
    }
    return(
        <div>
            <button onClick={login}>LOGIN</button>
        </div>
    );
}

export default LoginButton

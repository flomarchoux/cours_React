import {Increment, Decrement} from '../actions/counter'

const INITIAL_STATE = {
    count: 0
}

const Reducer = (state = INITIAL_STATE,action) => {
    switch(action.type){
        case Increment:
            return {
                count : state.count + 1
            }
        case Decrement:
            return {
                count : state.count - 1
            }
        default:
            return {
                count : state.count
            }
    }
}

export default Reducer
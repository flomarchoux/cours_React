import LoginControl from "./component/ControLogin";
import Counter from "./component/Counter";
import SplitUrl from "./component/spliturl"
import './style/main.css';
import Counter2 from "./connect/connect_1"
import Accueil from "./component/home"
import React, {Fragment} from "react"
import {BrowserRouter as Router, Route} from "react-router-dom"
import Footer from "./component/footer";
import Autre from "./component/about";


function App(props) {
    return (
        <Router>
            <main>
                <nav>
                    <a href="/"><button className="first" id="active">Home</button></a>
                    <a href="/ConnexionDeconnexion"><button className="classique">Greeting</button></a>
                    <a href="/Click"><button className="classique">Click</button></a>
                    <a href="/SplitUrl"><button className="classique">SplitUrl</button></a>
                    <a href="/Click2"><button className="classique">Click-Redux</button></a>
                    <a href="/about"><button className="last">Autre</button></a>
                </nav>

                <Route path="/" exact component={Home}/>
                <Route path="/ConnexionDeconnexion" component={Exo1}/>
                <Route path="/Click" component={Click}/>
                <Route path="/SplitUrl" component={split}/>
                <Route path="/Click2" component={Click2}/>
                <Route path="/about" component={About}/>
                <Footer/>
            </main>
        </Router>
    );
}

const Home = () => (
    <Fragment>
        <Accueil/>
    </Fragment>
)

const split = () => (
    <Fragment>
        <center>
            <table>
                <tr>
                    <th>Split URL</th>
                </tr>
                <tr>
                    <td><SplitUrl/></td>
                </tr>
            </table>
        </center>
    </Fragment>
)

const Click2 = () => (
    <Fragment>
        <center>
            <table>
                <tr>
                    <th>Click + et - avec REDUX</th>
                </tr>
                <tr>
                    <td><Counter2/></td>
                </tr>
            </table>
        </center>
    </Fragment>
)

const Click = () => (
    <Fragment>
        <center>
            <table>
                <tr>
                    <th>Click + et -</th>
                </tr>
                <tr>
                    <td><Counter/></td>
                </tr>
            </table>
        </center>
    </Fragment>
)

const Exo1 = () => (
    <Fragment>
        <center>
            <table>
                <tr>
                    <th>Connexion et Déconnexion</th>
                </tr>
                <tr>
                    <td><LoginControl/></td>
                </tr>
            </table>
        </center>
    </Fragment>
)

const About = () => (
    <Fragment>
        <Autre/>
    </Fragment>
)

export default App
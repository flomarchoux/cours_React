import {connect} from 'react-redux';
import Counter2 from "../component/Counter2";
import {countIncremente} from "../actions/counter";
import {countDecrease} from "../actions/counter";

function mapDispatchToProps(dispatch) {
    return {
        increase : () => dispatch(countIncremente()),
        decrease : () => dispatch(countDecrease())
    }
}

function mapStateToProps(state) {
    return {
        count : state.count
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter2);